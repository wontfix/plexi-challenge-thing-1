import discord
from discord.ext import commands
from core.config import config
import sys
from logbook import Logger, StreamHandler
from aiohttp import ClientSession

class GS(commands.AutoShardedBot):
    """
    A customized bot class for GameStatus.
    Subclass of discord.ext.commands.AutoShardedBot.
    Required Arguments:
      - config: File path to a valid YAML config for GameStatus.
      - command_prefix: Prefix for bot. Required by discord.py.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        StreamHandler(sys.stdout).push_application()
        self.log = Logger('gsbot')
        self.log.info('starting gamestatus (v0.0.1)')

        try:
            self.config = config(kwargs['config'])
            self.log.info('successfully loaded config')
        except Exception as e:
            self.log.error(e)
            sys.exit(1)

        self.req = ClientSession(loop=self.loop)

    async def on_ready(self):
        self.log.info('ready!')
