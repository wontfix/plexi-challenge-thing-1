from ruamel.yaml import YAML


def config(file):
    return YAML(typ='safe').load(open(file).read().rstrip())
