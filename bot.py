from core import client, config
import os

_c = config.config('config.yaml')

bot = client.GS(config='config.yaml', command_prefix=_c['prefix'])

for d, sd, files in os.walk('cogs'):
    for f in files:
        bot.log.info(f'attempting load of {f}')
        current = f.replace('.py', '')
        try:
            bot.load_extension(f'cogs.{current}')
        except Exception as e:
            bot.log.error(e)

bot.run(_c['token'])
