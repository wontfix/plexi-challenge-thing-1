from discord.ext import commands
import discord
from random import shuffle
import html


class Trivia:
    def __init__(self, bot):
        self.bot = bot
        self.api = 'https://opentdb.com/api.php?amount=1'

    @commands.command()
    async def trivia(self, ctx):
        def get_answer_desc(results):
            """
            Gets the answer type and returns a appropriate
            embed description.
            Returns: list
            Example: [desc, correct_answer]
            """
            if results['type'] == 'boolean':
                # If the type of trivia is boolean,
                # we go true or false.
                return ['True or False?', results['correct_answer']]
            elif results['type'] == 'multiple':
                # Multiple choice is a different story,
                # however. We have to get all the answers
                # and shuffle them around so the player
                # doesn't cheat (this would happen if the
                # correct answer's position was hardcoded).
                # Let's put in the correct answer first.
                answers = [results['correct_answer']]
                # Then load in the incorrect ones.
                for a in results['incorrect_answers']:
                    answers.append(a)

                # Then shuffle.
                shuffle(answers)

                # Now here's the fun part.
                # We make a embed description here (finally!).
                # First we make the variables.
                desc = ''

                # This for loop makes the description + the numbers.
                for g in answers:
                    desc = desc + f'- {g}\n'
                yes_answer = results['correct_answer']
                return [desc, yes_answer]

        def get_answer_type(results):
            """
            We could get away with a single function.
            But for cleaner code I'm choosing to do this.
            """
            if results['type'] == 'boolean':
                return 'True/False'
            if results['type'] == 'multiple':
                return 'Multiple Choice'

        async with ctx.typing():
            async with ctx.bot.req.get(self.api) as q:
                def check_answer(m):
                    """
                    This is a check for discord.py.
                    It only checks in the current channel,
                    and only uses messages from the person
                    who ran the command.
                    """
                    a = ctx.message.author
                    c = ctx.message.channel
                    return m.author == a and m.channel == c

                # Get JSON from aiohttp request.
                y = await q.json()
                y = y['results'][0]

                # I make this a variable,
                # because I return the list in get_answer_desc.
                # I don't want to call the function 2 times.
                # As it's a bit slow.
                answers = get_answer_desc(y)

                # Step by step:
                # The title is the difficulty and the question.
                # We use html.unescape because OpenTDB uses HTML
                # encoding in their responses for some reason.
                # The finished product is like: [hard] This is a question?
                # The description is the first thing we returned from
                # get_answer_desc, the list of answers we can use.
                # As well as a bit of instruction.
                e = discord.Embed(title=f'[{y["difficulty"]}]'
                                  f' {html.unescape(y["question"])}',
                                  description=answers[0] + '\nType your '
                                  'answer.')
                # Our footer is the answer type, and the category.
                e.set_footer(text=f'{get_answer_type(y)} - {y["category"]}')
                await ctx.send(embed=e)

        # And now we wait.
        answer = await ctx.bot.wait_for('message', check=check_answer)
        # Once we get a message that satisfies our requirements,
        # we check it against the JSON to see if right or wrong.
        if answer.content == answers[1]:
            await ctx.send(':white_check_mark: Yay! You got it right.')
        else:
            await ctx.send(':x: Better luck next time :<')
        # That's all!


def setup(bot):
    bot.add_cog(Trivia(bot))
