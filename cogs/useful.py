from discord.ext import commands
import traceback

class UsefulThings:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    @commands.is_owner()
    async def reload(self, ctx, *exts: str):
        for ext in exts:
            try:
                self.bot.unload_extension(f'cogs.{ext}')
                self.bot.load_extension(f'cogs.{ext}')
            except ModuleNotFoundError:
                return await ctx.send(':x: Cog not found.')
            except Exception:
                return await ctx.send(f'```{traceback.format_exc()}```')
            else:
                await ctx.send(':ok_hand: Reloaded cog.')

def setup(bot):
    bot.add_cog(UsefulThings(bot))
